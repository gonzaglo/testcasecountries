//
//  CountriesService.swift
//  NetKit
//
//  Created by Георгий Кузьминых on 25/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import Moya

public enum CountriesService {
    case getAll
    case getInfo(countryCode:String)
    case getNeighbors(codes:[String])
}

extension CountriesService: TargetType {
    
    public var baseURL: URL { return URL(string: "https://restcountries.eu/rest/v2")! }
    
    public var path: String {
        switch self {
        case .getAll:
            return "/all"
        case .getNeighbors:
            return "/alpha"
        case .getInfo(let countryCode):
            return "/alpha/\(countryCode)"
        }
    }
    
    public var method: Moya.Method {
        return .get
    }
    
    public var headers: [String : String]? {
        return nil
    }
    
    var parameters: [String: Any] {
        switch self {
        case .getAll:
            return ["fields":"name;population;alpha3Code"]
        case .getNeighbors(let codes):
            return ["codes":codes.joined(separator: ";")]
        case .getInfo: return [:]
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .getAll: return .requestPlain
        case .getNeighbors,.getInfo: return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
    }
}
