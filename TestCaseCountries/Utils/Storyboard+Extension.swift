//
//  Storyboard+Extension.swift
//  TestCaseCountries
//
//  Created by Георгий Кузьминых on 28/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import UIKit

extension UIStoryboard {
    func instantiateViewController<T:UIViewController>(withType: T.Type) -> T {
        let vc = self.instantiateViewController(withIdentifier: String(describing: T.self)) as! T
        return vc
    }
}
