//
//  Int+Extension.swift
//  TestCaseCountries
//
//  Created by Георгий Кузьминых on 27/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import Foundation

extension Int {
    func format() -> String{
        let fmt = NumberFormatter()
        fmt.numberStyle = .decimal
        fmt.locale = Locale(identifier: "fr_FR")
        return fmt.string(from: NSNumber(value: self)) ?? "\(self)"
    }
}
