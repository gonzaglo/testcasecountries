//
//  String+Extension.swift
//  TestCaseCountries
//
//  Created by Георгий Кузьминых on 28/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import Foundation

extension Optional where Wrapped == String {
    func or(default: String) -> String {
        if self?.isEmpty ?? true {
            return `default`
        }
        return self!
    }
}
