//
//  CountriesListViewModel.swift
//  TestCaseCountries
//
//  Created by Георгий Кузьминых on 25/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import UIKit
import Moya
import NetKit
import RxSwift
import RxCocoa
import RxDataSources

typealias CountrySection = SectionModel<String, Country>

class CountriesListViewModel:VMLoadable {
    
    private let serviceProvider = MoyaProvider<CountriesService>()
    private let disposeBag = DisposeBag()
    
    var countries = BehaviorRelay<[CountrySection]>(value: [])
    var isLoading = false
    
    func loadData(startBlock:()->Void, completion: @escaping (String?)->Void){
        if isLoading {return}
        isLoading = true
        startBlock()
        
        serviceProvider.rx.request(.getAll)
            .map(Array<Country>.self)
            .subscribe(onSuccess: { value in
                self.countries.accept(value.toSection())
                self.isLoading = false
                completion(nil)
            }, onError: { error in
                self.countries.accept([])
                self.isLoading = false
                let errorMessage = (error as? MoyaError)?.localizedDescription
                completion(errorMessage)
            }).disposed(by: disposeBag)
    }
    
}

private extension Array where Element: Country {
    func toSection() -> [CountrySection] {
        let dict = Dictionary(grouping: self, by: {String($0.name?.prefix(1) ?? "")})
        return dict.map{CountrySection(model: $0.key, items: $0.value)}.sorted{
            $0.model.localizedCaseInsensitiveCompare($1.model) == ComparisonResult.orderedAscending
        }
    }
}
