//
//  CountriesListController.swift
//  TestCaseCountries
//
//  Created by Георгий Кузьминых on 25/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class CountriesListController: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let viewModel = CountriesListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure(tableView)
        bind(to: viewModel)
        refreshData()
    }
    
    
    override func bind(to vm:VMLoadable) {
        super.bind(to: viewModel)
        
        let dataSource = configureDataSource()
        
        viewModel.countries
        .bind(to: tableView.rx.items(dataSource: dataSource))
        .disposed(by: disposeBag)
        
        Observable
            .zip(tableView.rx.itemSelected, tableView.rx.modelSelected(Country.self))
            .bind { [weak self] indexPath, model in
                guard let `self` = self else {return}
                self.tableView.deselectRow(at: indexPath, animated: true)
                if let code = model.alpha3Code{
                    let vc = UIStoryboard(name: "CountryInfo", bundle: nil)
                        .instantiateViewController(withType: CountryInfoController.self)
                    
                    vc.viewModel = CountryInfoViewModel(countryCode: code)
                    vc.title = model.name
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
            .disposed(by: disposeBag)
        
    }
    
    private func configureDataSource() -> RxTableViewSectionedReloadDataSource<CountrySection>{
        
        return RxTableViewSectionedReloadDataSource<CountrySection>(
            configureCell: { (_, tv, indexPath, country) in
                let cell = tv.dequeueReusableCell(with: CountryPreviewCell.self, for: indexPath)
                cell.countryName.text = country.name
                cell.populationValue.text = country.population?.format()
                return cell
        },
            titleForHeaderInSection: { dataSource, sectionIndex in
                return dataSource[sectionIndex].model
        }
        )
    }
    
    override func configure(_ tableView: UITableView) {
        super.configure(tableView)
        tableView.register(cell: CountryPreviewCell.self)
    }
    
}
