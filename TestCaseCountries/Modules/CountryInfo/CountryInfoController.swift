//
//  CountryInfoController.swift
//  TestCaseCountries
//
//  Created by Георгий Кузьминых on 25/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import UIKit

class CountryInfoController: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    var viewModel:CountryInfoViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure(tableView)
        bind(to: viewModel)
        refreshData()
    }
    
    override func bind(to vm:VMLoadable) {
        super.bind(to: viewModel)
        viewModel.countryInfo
            .asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: String(describing: InfoCell.self), cellType: InfoCell.self)){
                _, info, cell in
                
                cell.titleLabel.text = info.title
                cell.valueLabel.text = info.value.or(default: "No")
                
            }.disposed(by: disposeBag)
    }
    
    override func configure(_ tableView: UITableView) {
        super.configure(tableView)
        tableView.register(cell: InfoCell.self)
    }
    
}
