//
//  CountryInfoViewModel.swift
//  TestCaseCountries
//
//  Created by Георгий Кузьминых on 27/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import Moya
import NetKit

class CountryInfoViewModel: VMLoadable{
    
    private var countryCode:String
    
    var countryProvider = MoyaProvider<CountriesService>()
    var isLoading = false
    
    private var country:Country?
    typealias CountryInfo = (title:String, value:String?)
    var countryInfo = BehaviorRelay<[CountryInfo]>(value: [])
    
    private let disposeBag = DisposeBag()
    
    init(countryCode:String) {
        self.countryCode = countryCode
    }
    
    func loadData(startBlock: () -> Void, completion: @escaping (String?) -> Void) {
        if isLoading {return}
        isLoading = true
        startBlock()
        
        countryProvider.rx.request(.getInfo(countryCode: countryCode))
            .map(Country.self)
            .subscribe(onSuccess: {[weak self] value in
                guard let `self` = self else {return}
                self.country = value
                self.loadCountryNeighbors(completion: completion)
            }, onError: {[weak self] error in
                guard let `self` = self else {return}
                self.countryInfo.accept([])
                self.isLoading = false
                let errorMessage = (error as? MoyaError)?.localizedDescription
                completion(errorMessage)
            }).disposed(by: disposeBag)
    }
    
    private func loadCountryNeighbors(completion: @escaping (String?) -> Void){
        
        guard let borders = country?.borders else {
            self.countryInfo.accept([])
            completion("Request error.")
            return
        }
        
        if borders.isEmpty {
            self.countryInfo.accept(self.configureInfo())
            self.isLoading = false
            completion(nil)
            return
        }
        
        countryProvider.rx.request(.getNeighbors(codes: borders))
        .map(Array<Country>.self)
        .subscribe(onSuccess: {[weak self] value in
            guard let `self` = self else {return}
            self.country?.neighbors = value.compactMap{$0.name}
            self.countryInfo.accept(self.configureInfo())
            self.isLoading = false
            completion(nil)
            }, onError: {[weak self] error in
                guard let `self` = self else {return}
                self.countryInfo.accept([])
                self.isLoading = false
                let errorMessage = (error as? MoyaError)?.localizedDescription
                completion(errorMessage)
        }).disposed(by: disposeBag)
    }
    
    private func configureInfo() -> [CountryInfo]{
        guard let country = country else {
            return []
        }
        var info:[CountryInfo] = []
        info.append(("Title", country.name))
        info.append(("Capital", country.capital))
        info.append(("Population", country.population?.format()))
        info.append(("Neighboring countries", country.neighbors?.joined(separator: ", ")))
        info.append(("Currencies", country.currencies?.compactMap{$0.toString()}.joined(separator: ", ")))
        
        return info
    }
    
}
