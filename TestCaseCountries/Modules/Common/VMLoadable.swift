//
//  VMLoadable.swift
//  TestCaseCountries
//
//  Created by Георгий Кузьминых on 29/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import Foundation

protocol VMLoadable {
    func loadData(startBlock: () -> Void, completion: @escaping (String?) -> Void)
}
