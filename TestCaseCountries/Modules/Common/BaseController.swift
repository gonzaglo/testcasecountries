//
//  BaseController.swift
//  TestCaseCountries
//
//  Created by Георгий Кузьминых on 27/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BaseController: UIViewController {
    
    private var _tableView:UITableView?
    private var _viewModel:VMLoadable?
    
    lazy var alertDialog: UIAlertController =  {
        let alert = UIAlertController(title: "Error", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: {[weak self] action in
            self?.refreshData()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        return alert
    }()
    
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func bind(to vm:VMLoadable) {
        self._viewModel = vm
    }
    
    func refreshData(){
        guard let _viewModel = _viewModel,
        let _tableView = _tableView else {return}
        _viewModel.loadData(startBlock: {
            _tableView.refreshControl?.beginRefreshing()
            let yOffset = _tableView.refreshControl?.frame.size.height ?? 40
            _tableView.setContentOffset(CGPoint(x: 0, y: -yOffset), animated: true)
            }, completion: {[weak self] error in
                guard let `self` = self else {return}
                if let error = error {
                    self.showAlert(with: error)
                }
                _tableView.reloadData()
                _tableView.refreshControl?.endRefreshing()
                _tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        })
    }
    
    func showAlert(with error:String){
        alertDialog.message = error
        self.present(alertDialog, animated: true)
    }
    
    @objc private func handleRefresh(_ refreshControl: UIRefreshControl){
        refreshData()
        
    }
    
    func configure(_ tableView:UITableView) {
        _tableView = tableView
        tableView.tableFooterView = UIView()
        tableView.refreshControl = {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(handleRefresh(_:)),
                                     for: UIControlEvents.valueChanged)
            return refreshControl
        }()
        
        
    }
}
