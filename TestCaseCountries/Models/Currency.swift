//
//  Currency.swift
//  TestCaseCountries
//
//  Created by Георгий Кузьминых on 25/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import UIKit

class Currency: Codable {
    var name:String?
    var symbol:String?
    
    func toString()->String?{
        guard let name = name,
            let symbol = symbol else {
            return nil
        }
        return "\(name)(\(symbol))"
    }
}
