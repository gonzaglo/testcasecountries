//
//  Country.swift
//  TestCaseCountries
//
//  Created by Георгий Кузьминых on 25/04/2019.
//  Copyright © 2019 Георгий Кузьминых. All rights reserved.
//

import UIKit

class Country: Codable {
    var name:String?
    var population:Int?
    var capital:String?
    var currencies:[Currency]?
    var alpha3Code:String?
    var borders:[String]?
    var neighbors:[String]?
}
